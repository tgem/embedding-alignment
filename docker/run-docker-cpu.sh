#!/bin/bash

docker run -d \
  --name dynalign \
  -v /home/docker/dynalign/:/dynalign \
  dynalign \
  /bin/bash -c "trap : TERM INT; sleep infinity & wait"