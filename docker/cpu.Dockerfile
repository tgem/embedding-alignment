FROM ubuntu:20.04

ADD . /dynalign
WORKDIR /dynalign

RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
RUN apt-get install -y $(awk '{print $1}' docker/apt.packages) && add-apt-repository ppa:deadsnakes/ppa && apt update

RUN apt-get install -y --no-install-recommends \
        python3.7 \
        python3.7-dev

RUN wget https://bootstrap.pypa.io/get-pip.py &&  \
	 python3.7 get-pip.py && \
	 ln -s /usr/bin/python3.7 /usr/local/bin/python3

RUN apt-get install -y zsh && \
    chsh -s /bin/zsh && \
    zsh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

RUN git remote set-url origin https://gitlab.com/tgem/dynalign.git

RUN pip install -r ./requirements.txt
RUN pip install -r ./requirements-torch-geometric-cpu.txt

CMD exec /bin/zsh -c "trap : TERM INT; sleep infinity & wait"
