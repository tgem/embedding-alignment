# Embedding alignment methods in dynamic networks
This is the official code for the article "Embedding alignment methods in dynamic networks" (Kamil Tagowski, Piotr Bielak, Tomasz Kajdanowicz). Accepted at ICCS 2021.

**Full Paper**: TBA  
**Short Paper** Accepted at GLB'21 Workshop: [[PDF]](https://graph-learning-benchmarks.github.io/assets/papers/GLB_Embedding_alignment_methods_in_dynamic_networks.pdf)  
**Spotlight Talk**: TBA   
**Poster**: TBA


# Requirements
All experiments were performed using Python 3.7, and for GPU computations we used CUDA 10.1. 
A list of required Python packages can be found in:
* general libraries - [requirements.txt](requirements.txt)
* CPU training - [requirements-cpu.txt](requirements-torch-geometric-cpu.txt)
* GPU training - [requirements-torch-geometric-gpu.txt](/requirements-torch-geometric-gpu.txt).
We also provide ready-to-use Docker environments, both for CPU and GPU-based training.

# Reproducibility
We employ [DVC](https://dvc.org/) pipelines for reproducible experiments. All scripts and their configurations can be found in the `experiments/` folder.
The pipeline definition can be found in the [dvc.yaml](dvc.yaml) file.  We share source graphs via google drive (same output as via `/data/raw/real.dvc` stage) [[LINK]](https://drive.google.com/file/d/1Srx20_aifw7d5tOwQq2xwneO2SewEgNL/view?usp=sharing). After download of source graphs, and putting it in `data/raw/real/` directory, experiments can be easily reproducible via `dvc repro` command.


# Contact
Currently, our remote DVC repository is not publicly available, but if you are interested in any kind of pipeline data artifacts, please contact us:
* Kamil `kamil [dot] tagowski [at] pwr [dot] edu [dot] pl`
* Piotr `piotr [dot] bielak [at] pwr [dot] edu [dot] pl`
(Replace the `[dot]`s and `[at]`s with proper punctuation)

