from dynalign import aligners as alg
from dynalign import regressors as reg


DATASET_SPECIFIC_PARAMS = {
    'bitcoin-alpha': {'percent': 0.9},
    'bitcoin-otc': {'percent': 0.9},
    'fb-forum': {'percent': 0.9},
    'fb-messages': {'percent': 0.5},
    'ia-enron-employees': {'percent': 0.9},
    'ia-hypertext': {'percent': 0.2},
    'ia-radoslaw-email': {'percent': 0.9},
    'ppi': {'percent': 0.5},
    'ogbl-collab': {'percent': 0.8},
}


ALIGNERS = {
    dataset: {
        'name': f'TemporalCloseness_{str(round(params["percent"], 1))}',
        'group': 'TemporalCloseness',
        'cls': alg.TemporalCentralityMeasureAligner,
        'cfg': {
            'regressor': reg.OrthogonalProcrustesRegressor,
            'reg_args': {},
            'percent': params['percent'],
            'centrality_measure_name': 'closeness',
            'verbose': False,
        }
    }
    for dataset, params in DATASET_SPECIFIC_PARAMS.items()
}
