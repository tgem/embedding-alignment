from dynalign import aligners as alg
from dynalign import regressors as reg
from dynalign import selectors as sel


DATASET_SPECIFIC_PARAMS = {
    'bitcoin-alpha': {'n': 0.1, 'percent': 0.5},
    'bitcoin-otc': {'n': 0.2, 'percent': 0.9},
    'fb-forum': {'n': 0.4, 'percent': 0.9},
    'fb-messages': {'n': 0.1, 'percent': 0.9},
    'ia-enron-employees': {'n': 0.9, 'percent': 0.9},
    'ia-hypertext': {'n': 0.1, 'percent': 0.9},
    'ia-radoslaw-email': {'n': 0.1, 'percent': 0.7},
    'ppi': {'n': 1.0, 'percent': 0.9},
    'ogbl-collab': {'n': 0.30000000000000004, 'percent': 0.7},
}


ALIGNERS = {
    dataset: {
        'name': f'EmbeddingNeighborJaccardPercent'
        f'_n_{str(round(params["n"], 2))}'
        f'_{str(round(params["percent"], 2))}',
        'group': 'EmbeddingNeighborJaccardPercent',
        'cls': alg.CustomDynAligner,
        'cfg': {
            'nodes_selector': {
                'cls': sel.EmbeddingNeighborJaccard,
                'args': {
                    'neighbors_percent': params['n'],
                    'selection_method': 'percent',
                    'selection_method_args': {
                        'percent': params['percent'],
                    }
                },
            },
            'regressor': reg.OrthogonalProcrustesRegressor,
            'reg_args': {}
        }
    }
    for dataset, params in DATASET_SPECIFIC_PARAMS.items()
}
