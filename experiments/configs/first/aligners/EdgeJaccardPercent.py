from dynalign import aligners as alg
from dynalign import regressors as reg
from dynalign import selectors as sel


DATASET_SPECIFIC_PARAMS = {
    'bitcoin-alpha': {'percent': 0.5},
    'bitcoin-otc': {'percent': 0.8},
    'fb-forum': {'percent': 0.9},
    'fb-messages': {'percent': 0.9},
    'ia-enron-employees': {'percent': 0.9},
    'ia-hypertext': {'percent': 0.4},
    'ia-radoslaw-email': {'percent': 0.7},
    'ppi': {'percent': 0.8},
    'ogbl-collab': {'percent': 0.9},
}


ALIGNERS = {
    dataset: {
        'name': f'EdgeJaccardPercent_{str(round(params["percent"], 1))}',
        'group': 'EdgeJaccardPercent',
        'cls': alg.CustomDynAligner,
        'cfg': {
            'nodes_selector': {
                'cls': sel.EdgeJaccardNodesSelector,
                'args': {
                    'selection_method': 'percent',
                    'selection_method_args': {
                        'percent': params['percent'],
                    }
                }
            },
            'regressor': reg.OrthogonalProcrustesRegressor,
            'reg_args': {}
        }
    }
    for dataset, params in DATASET_SPECIFIC_PARAMS.items()
}
