from dynalign import aligners as alg
from dynalign import regressors as reg


ALIGNERS = {
    'name': 'Procrustes',
    'group': 'Procrustes',
    'cls': alg.BaseAligner,
    'cfg': {
        'regressor': reg.OrthogonalProcrustesRegressor,
        'reg_args': {}
    },
}
