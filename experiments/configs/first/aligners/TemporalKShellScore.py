from dynalign import aligners as alg
from dynalign import regressors as reg


DATASET_SPECIFIC_PARAMS = {
    'bitcoin-alpha': {'percent': 0.5},
    'bitcoin-otc': {'percent': 0.9},
    'fb-forum': {'percent': 0.8},
    'fb-messages': {'percent': 0.9},
    'ia-enron-employees': {'percent': 0.9},
    'ia-hypertext': {'percent': 0.4},
    'ia-radoslaw-email': {'percent': 0.7},
    'ppi': {'percent': 0.8},
    'ogbl-collab': {'percent': 0.7},
}


ALIGNERS = {
    dataset: {
        'name': f'TemporalKShellScore_{str(round(params["percent"], 1))}',
        'group': 'TemporalKShellScore',
        'cls': alg.TemporalCentralityMeasureAligner,
        'cfg': {
            'regressor': reg.OrthogonalProcrustesRegressor,
            'reg_args': {},
            'percent': params['percent'],
            'centrality_measure_name': 'k_shell_score',
            'verbose': False,
        }
    }
    for dataset, params in DATASET_SPECIFIC_PARAMS.items()
}
