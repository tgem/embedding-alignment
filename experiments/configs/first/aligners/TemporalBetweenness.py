from dynalign import aligners as alg
from dynalign import regressors as reg


DATASET_SPECIFIC_PARAMS = {
    'bitcoin-alpha': {'percent': 0.8},
    'bitcoin-otc': {'percent': 0.9},
    'fb-forum': {'percent': 0.8},
    'fb-messages': {'percent': 0.8},
    'ia-enron-employees': {'percent': 0.9},
    'ia-hypertext': {'percent': 0.6},
    'ia-radoslaw-email': {'percent': 0.9},
    'ppi': {'percent': 0.9},
    'ogbl-collab': {'percent': 0.9},
}


ALIGNERS = {
    dataset: {
        'name': f'TemporalBetweenness_{str(round(params["percent"], 1))}',
        'group': 'TemporalBetweenness',
        'cls': alg.TemporalCentralityMeasureAligner,
        'cfg': {
            'regressor': reg.OrthogonalProcrustesRegressor,
            'reg_args': {},
            'percent': params['percent'],
            'centrality_measure_name': 'betweenness',
            'verbose': False,
        }
    }
    for dataset, params in DATASET_SPECIFIC_PARAMS.items()
}
