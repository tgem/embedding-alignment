from dynalign import aligners as alg
from dynalign import regressors as reg


ALIGNERS = {
    'name': 'ProcrustesUnchanged',
    'group': 'ProcrustesUnchanged',
    'cls': alg.DynAligner,
    'cfg': {
        'regressor': reg.OrthogonalProcrustesRegressor,
        'reg_args': {}
    },
}
