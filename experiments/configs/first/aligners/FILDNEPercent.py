from dynalign import aligners as alg
from dynalign import regressors as reg


DATASET_SPECIFIC_PARAMS = {
    'bitcoin-alpha': {'percent': 0.4},
    'bitcoin-otc': {'percent': 0.8},
    'fb-forum': {'percent': 0.9},
    'fb-messages': {'percent': 0.9},
    'ia-enron-employees': {'percent': 0.9},
    'ia-hypertext': {'percent': 0.9},
    'ia-radoslaw-email': {'percent': 0.9},
    'ppi': {'percent': 0.2},
    'ogbl-collab': {'percent': 0.9},
}


ALIGNERS = {
    dataset: {
        'name': f'FILDNE_{str(round(params["percent"], 1))}',
        'group': 'FILDNEPercent',
        'cls': alg.FILDNEPercentAligner,
        'cfg': {
            'percent': params['percent'],
            'regressor': reg.OrthogonalProcrustesRegressor,
            'reg_args': {}
        }
    }
    for dataset, params in DATASET_SPECIFIC_PARAMS.items()
}
