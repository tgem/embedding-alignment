import argparse
import contextlib
import json
import os
from functools import partial

import hyperopt as hopt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml
from tqdm import tqdm

import dynalign.evaluation
from dynalign import n2v as node2vec
from dynalign.embeddings import KeyedModel
from dynalign.evaluation import LogisticRegressionModel


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-d",
        "--dataset",
        help="Name of the dataset",
    )

    parser.add_argument(
        "-gd",
        "--graphs-dir",
        help="Path to directory with input nx graphs",
        default="data/graphs",
    )
    parser.add_argument(
        "-tgd",
        "--tg-graphs-dir",
        help="Path to directory with input tg graphs",
        default="data/tg_graphs",
    )
    parser.add_argument(
        "-n",
        "--num-iterations",
        help="Number of HyperOpt iterations",
        type=int,
    )

    parser.add_argument(
        "-od",
        "--output-dir",
        help="Path to directory with output artifacts",
        default="data/node2vec_hps",
    )

    return parser.parse_args()


def compute_auc(
    embedding: KeyedModel,
    lp_ds: dynalign.evaluation.LinkPredictionDataset,
) -> float:
    lrm = LogisticRegressionModel(embedding=embedding)
    lrm.fit(x=lp_ds.x_train, y=lp_ds.y_train)

    return lrm.validate(x=lp_ds.x_test, y=lp_ds.y_test).as_dict()["auc"].item()


def estimate_calculation_time(first_snapshot_time, snapshots_len):
    return first_snapshot_time * snapshots_len


def evaluate_node2vec_lp(graphs, lp_datasets, node_mappings, n2v_cfg) -> dict:
    aucs = []
    cl_times = []
    
    print(n2v_cfg)
    max_calculation_time = n2v_cfg.pop("max_calculation_time")
    for graph_id, graph in tqdm(
            iterable=enumerate(graphs), desc="Embeddings", total=len(graphs),
            leave=False
    ):
        try:
            emb, cl = node2vec.Node2VecEmbedding(
                **n2v_cfg, node_index_mapping=node_mappings[graph_id]
            ).embed(graph=graph)
        except Exception as e:
            return {
                "status": hopt.STATUS_FAIL,
                "error": "Embedding training error",
                "error_message": f"{e}"
            }

        # Ignore too long runs
        if graph_id == 0:
            es = estimate_calculation_time(cl, len(graphs))
            if es >= max_calculation_time:
                return {
                    "status": hopt.STATUS_FAIL,
                    "error": "Calculation time limit exceeded",
                    "error_message": (
                        f"Estimated total calculation time: {es}, "
                        f"Limit: {max_calculation_time}"
                    )
                }

        aucs.append(
            compute_auc(embedding=emb, lp_ds=lp_datasets[graph_id])
        )
        cl_times.append(cl)
    
    mean_auc = np.mean(aucs).item()
    total_cl_time = np.sum(cl_times).item()
    return {
        "loss": -1 * mean_auc,  # The loss is always minimized
        "aucs": aucs,
        "mean_auc": mean_auc,
        "total_calculation_time": total_cl_time,
        "status": hopt.STATUS_OK,
    }


class ConfigSpace:
    GRAPH_PARAMS_SPACE_MAPPING = {
        'bitcoin-alpha': 'very-small',
        'bitcoin-otc': 'small',
        'fb-forum': 'small',
        'fb-messages': 'small',
        'ia-hypertext': 'very-small',
        'ia-radoslaw-email': 'very-small',
        'ia-enron-employees': 'very-small',
        'ppi': 'medium',
        'ogbl-collab': 'large',
    }

    GRAPH_PARAMS_SPACE = {
        'very-small': {
            'dimension': 32,
            'walk_length': [10, 20, 50, 80, 100],
            'nb_walks_per_node': [1, 5, 10, 20, 50, 100, 200],
            'w2v_epochs': [1, 2, 5, 10, 20, 50, 100],
            'context_size': [3, 5, 10, 15, 20],
            'batch_size': [1, 2, 4, 8, 16],
            'lr': [1e-5, 1e-4, 1e-3, 1e-2],
            'max_calculation_time': 250
        },
        'small': {
            'dimension': 128,
            'walk_length': [10, 20, 50, 80, 100],
            'nb_walks_per_node': [1, 5, 10, 20, 50, 100],
            'w2v_epochs': [1, 2, 5, 10, 20, 30],
            'context_size': [3, 5, 10, 15, 20],
            'batch_size': [16, 32, 64],
            'lr': [1e-5, 1e-4, 1e-3, 1e-2],
            'max_calculation_time': 300
        },
        'medium': {
            'dimension': 128,
            'walk_length': [10, 20, 50, 80],
            'nb_walks_per_node': [1, 2, 5, 10, 15, 20],
            'w2v_epochs': [1, 2, 5, 10, 20, 30],
            'context_size': [3, 5, 10, 15, 20],
            'batch_size': [64, 128, 256],
            'lr': [1e-5, 1e-4, 1e-3, 1e-2],
            'max_calculation_time': 500
        },
        'large': {
            'dimension': 128,
            'walk_length': [10, 20, 40, 50, 60],
            'nb_walks_per_node': [1, 2, 5, 10, 15],
            'w2v_epochs': [1, 2, 3],
            'context_size': [3, 5, 10, 15, 20],
            'batch_size': [128, 256, 512],
            'lr': [1e-5, 1e-4, 1e-3, 1e-2],
            'max_calculation_time': 500
        }
    }

    def __init__(self, name: str):
        # TODO: Some values should depend on the dataset, like dimensions
        self.params_space = self.GRAPH_PARAMS_SPACE[
            self.GRAPH_PARAMS_SPACE_MAPPING[name]
        ]

        self._choice_values = {
            "walk_length": self.params_space['walk_length'],
            "nb_walks_per_node": self.params_space['nb_walks_per_node'],
            "batch_size": self.params_space['batch_size'],
            "lr": self.params_space['lr'],
            "context_size": self.params_space['context_size'],
            "w2v_epochs": self.params_space['w2v_epochs'],
        }

        self._deterministic_config = {
            "dimensions": self.params_space['dimension'],
            "max_calculation_time": self.params_space['max_calculation_time'],
            "quiet": True,
        }

    def get_space(self) -> dict:
        cs = self._deterministic_config.copy()

        for name, values in self._choice_values.items():
            cs[name] = hopt.hp.choice(name, values)

        cs["p"] = hopt.hp.quniform("p", 1.0, 4.0, 0.25)
        cs["q"] = hopt.hp.quniform("q", 1.0, 4.0, 0.25)

        return cs

    def from_hypeopt_result(self, result: dict) -> dict:
        params = self._deterministic_config.copy()

        for name, idx in result.items():
            if name in ("p", "q"):
                params[name] = idx.item()
            elif name == "max_calculation_time":
                continue
            else:
                params[name] = self._choice_values[name][idx]

        return params


@contextlib.contextmanager
def get_progress_bar(initial, total):
    with tqdm(
        total=total,
        postfix={"best loss": "?"},
        dynamic_ncols=True,
        unit="trial",
        initial=initial,
    ) as pbar:
        yield pbar


def transform_trials(trials, config_space):
    # Prepare execution history
    history = [
        {
            "params": config_space.from_hypeopt_result({
                k: v[0]
                for k, v in trial["misc"]["vals"].items()
            }),
            "result": trial["result"],
        }
        for trial in trials.trials
    ]

    # Prepare best trial
    best = {
        "result": trials.best_trial["result"],
        "params": config_space.from_hypeopt_result({
            k: v[0]
            for k, v in trials.best_trial["misc"]["vals"].items()
        }),
    }

    return history, best


def save_results(output_path, history, best):
    # Make AUC in time plot
    fig, ax = plt.subplots(figsize=(10, 10))
    x, y = zip(*[
        (idx, h["result"]["mean_auc"])
        for idx, h in enumerate(history)
        if h["result"]["status"] == hopt.STATUS_OK
    ])
    ax.plot(x, y, linestyle='--', marker='o')
    ax.set_xlabel("Step")
    ax.set_ylabel("Mean AUC")
    ax.set_title("Mean AUC over steps")
    ax.set_xticks(range(len(history)))
    ax.set_ylim((0, 1))

    # Save to files
    os.makedirs(output_path, exist_ok=True)
    with open(os.path.join(output_path, "best_params.yaml"), "w") as fout:
        yaml.safe_dump(data=best, stream=fout)

    with open(os.path.join(output_path, "history.jl"), "w") as fout:
        for h in history:
            fout.write(json.dumps(h) + "\n")

    fig.savefig(os.path.join(output_path, "mean_AUC.png"))


def main():
    args = get_args()

    dataset_name = args.dataset
    nx_graphs = pd.read_pickle(
        os.path.join(args.graphs_dir, f"{dataset_name}.pkl")
    )[dataset_name]["graphs"]

    lp_datasets = [
        dynalign.evaluation.LinkPredictionDataset().mk_link_prediction_dataset(
            graph=graph,
            split_proportion=0.75,
        )
        for graph in nx_graphs
    ]

    tg_data = pd.read_pickle(
        os.path.join(args.tg_graphs_dir, f"{dataset_name}.pkl")
    )[dataset_name]
    tg_graphs = tg_data["graphs"]
    node_mappings = tg_data["node_mappings"]

    trials = hopt.base.Trials()
    algo = hopt.tpe.suggest

    fn = partial(evaluate_node2vec_lp, tg_graphs, lp_datasets, node_mappings)
    cs = ConfigSpace(name=dataset_name)
    num_iterations = args.num_iterations

    hopt.fmin(
        fn=fn,
        space=cs.get_space(),
        max_evals=num_iterations,
        algo=algo,
        trials=trials,
        show_progressbar=get_progress_bar,
    )
    history, best = transform_trials(
        trials=trials,
        config_space=cs,
    )

    save_results(
        output_path=os.path.join(args.output_dir, dataset_name),
        history=history,
        best=best,
    )


if __name__ == "__main__":
    main()
