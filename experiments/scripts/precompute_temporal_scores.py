import argparse
import os
import pickle

import pandas as pd
import yaml
from tqdm import tqdm

from dynalign import preprocess as dpp
from dynalign import temporal_scores as dts
from dynalign.aligners import get_common_nodes


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-d',
        '--dataset',
        help='Name of the dataset',
        type=str,
        required=True,
    )

    return parser.parse_args()


def main():
    args = get_args()

    config_file = 'experiments/configs/first/precompute_temporal_scores.yaml'

    with open(config_file, 'r') as fin:
        cfg = yaml.safe_load(fin)

    dataset_name = args.dataset

    graphs_path = cfg['paths']['input'].replace('${dataset}', dataset_name)
    cached_path = cfg['paths']['output'].replace('${dataset}', dataset_name)

    # Get timestamp format
    timestamp_format_year = False
    if dataset_name in cfg['timestamp_format_year']:
        timestamp_format_year = True

    # Read graphs
    datasets = pd.read_pickle(graphs_path)

    metric_fns = [
        ('closeness', dts.closeness),
        ('betweenness', dts.betweenness),
        ('k_shell_score', dts.k_shell_score),
        ('degree_deviation', dts.degree_deviation),
    ]

    temporal_metrics = {
        metric_name: {}
        for metric_name, _ in metric_fns
    }

    pbar = tqdm(iterable=metric_fns, desc='Metric')

    for metric_name, metric_fn in pbar:
        pbar.set_description(f'Metric: {metric_name}')

        for dataset_id in tqdm(datasets.keys(), desc='Dataset', leave=False):
            values = []
            graphs = datasets[dataset_id]['graphs']
            preprocessed_graphs = [
                dpp.preprocess_graph(
                    dataset_name=dataset_name,
                    graph=graph,
                    timestamp_format_year=timestamp_format_year
                )
                for graph in graphs
            ]

            for idx, graph in tqdm(
                iterable=enumerate(graphs[1:], start=1),
                desc='Graph',
                total=len(graphs) - 1,
                leave=False,
            ):
                common_nodes = get_common_nodes(
                    g_u=graphs[0],
                    g_v=graph,
                )

                g_u = preprocessed_graphs[0]
                g_v = preprocessed_graphs[idx]

                s_u = metric_fn(graph=g_u, nodes=common_nodes, verbose=False)
                s_v = metric_fn(graph=g_v, nodes=common_nodes, verbose=False)

                values.append((s_u, s_v))

            temporal_metrics[metric_name][dataset_id] = values

    pbar.close()

    # Save results
    os.makedirs(os.path.dirname(cached_path), exist_ok=True)
    with open(cached_path, 'wb') as f:
        pickle.dump(obj=temporal_metrics, file=f)


if __name__ == '__main__':
    main()
