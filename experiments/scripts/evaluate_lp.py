import argparse
import os
import pickle
import numpy as np
import pandas as pd
import yaml
from tqdm import tqdm

from dynalign import embeddings as agg
from dynalign.evaluation import compute_auc


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-a',
        '--aligner',
        help='Name of aligner',
        type=str,
        required=True,
    )

    return parser.parse_args()


def evaluate_lp(graphs, embeddings, agg_model):
    AGG_MODEL_CLS = agg.EMBEDDING_AGGREGATION_MODELS[agg_model]
    results = {}

    for key in tqdm(graphs.keys(), desc='Dataset', leave=False):
        res = []
        for lp_ds_id, lp_ds in enumerate(graphs[key]['lp_ds'][2:]):
            for embs in embeddings[key]:
                emb_arr = embs['embeddings'][:(lp_ds_id + 2)]
                auc = np.nan

                if all(emb_arr):
                    auc = compute_auc(
                        embeddings=emb_arr,
                        lp_ds=lp_ds,
                        embedding_agg_model=AGG_MODEL_CLS(),
                    )

                res.append({
                    'run': embs['run'],
                    'aligner': embs['aligner'],
                    'group': embs['group'],
                    'lp_ds_id': lp_ds_id + 2,
                    'auc': auc,
                })

        results[key] = res

    return results


def main():
    args = get_args()

    config_file = 'experiments/configs/first/evaluate_lp_avg.yaml'

    with open(config_file, 'r') as fin:
        cfg = yaml.safe_load(fin)

    aligner_group = args.aligner

    graphs_path = cfg['paths']['input']['graphs']
    embeddings_path = cfg['paths']['input']['embeddings']
    aligned_emb_path = cfg['paths']['input']['aligned_embeddings']
    results_path = cfg['paths']['output'].replace('${ALIGNER}', aligner_group)

    lp_results = {}

    for dataset in cfg['datasets']:
        graphs = pd.read_pickle(graphs_path.replace('${dataset}', dataset))

        if aligner_group == 'None':
            embeddings = pd.read_pickle(
                embeddings_path.replace('${dataset}', dataset)
            )
            embeddings = {
                k: [
                    {
                        'run': int(ek.replace('embedding_', '')),
                        'aligner': 'None',
                        'group': aligner_group,
                        'embeddings': ev,
                    }
                    for ek, ev in v.items()
                ]
                for k, v in embeddings.items()
            }
        else:
            embeddings = pd.read_pickle(
                aligned_emb_path
                .replace('${ALIGNER}', aligner_group)
                .replace('${dataset}', dataset)
            )
            embeddings = {
                k: [
                    {**vals, 'embeddings': vals['aligned_embedding']}
                    for vals in v
                ]
                for k, v in embeddings.items()
            }

        res = evaluate_lp(
            graphs=graphs,
            embeddings=embeddings,
            agg_model=cfg['model'],
        )
        lp_results.update(res)

    os.makedirs(os.path.dirname(results_path), exist_ok=True)
    with open(results_path, 'wb') as f:
        pickle.dump(obj=lp_results, file=f)


if __name__ == '__main__':
    main()
