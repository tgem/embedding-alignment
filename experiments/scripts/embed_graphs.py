import os
import pickle
import sys
import yaml
from copy import deepcopy

import pandas as pd
from tqdm import tqdm
from dynalign.n2v import Node2VecEmbedding


def embed_all(datasets, node_mappings, num_repeats, cfg):
    embeddings = {}
    emb_cfg = deepcopy(cfg)

    for k in tqdm(datasets.keys(), desc='Datasets'):
        embeddings[k] = {
            f'embedding_{i}': [
                Node2VecEmbedding(
                    **emb_cfg,
                    node_index_mapping=node_mapping
                ).embed(graph=graph)
                for graph, node_mapping in tqdm(
                    iterable=zip(datasets[k], node_mappings[k]),
                    total=len(datasets[k]),
                    desc='Snapshot',
                    leave=False,
                )
            ]
            for i in tqdm(range(num_repeats), desc='Repeats', leave=False)
        }

    return embeddings


def main():
    # Read config
    with open('experiments/configs/embed_graphs.yaml', 'r') as fin:
        cfg = yaml.safe_load(fin)

    dataset = sys.argv[1]

    # Get config values
    embeddings_path = cfg['paths']['output'].replace('${dataset}', dataset)
    num_repeats = cfg['num_repeats'][dataset]
    emb_cfg = cfg['node2vec'][dataset]

    dataset_path = cfg['paths']['tg_input'].replace('${dataset}', dataset)

    # Read
    datasets = {
        k: v['graphs']
        for k, v in pd.read_pickle(dataset_path).items()
    }
    node_mappings = {
        k: v['node_mappings']
        for k, v in pd.read_pickle(dataset_path).items()
    }

    # Embed
    embeddings = embed_all(
        datasets=datasets,
        node_mappings=node_mappings,
        num_repeats=num_repeats,
        cfg=emb_cfg,
    )

    # Save
    os.makedirs(os.path.dirname(embeddings_path), exist_ok=True)
    with open(embeddings_path, 'wb') as f:
        pickle.dump(obj=embeddings, file=f)


if __name__ == '__main__':
    main()
