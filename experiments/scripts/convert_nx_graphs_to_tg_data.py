"""Script for converting nx data to tg data."""
import os
import pickle
import sys

import networkx as nx
import yaml

from dynalign.tg_utils import preprocess_graph_n2v_tg


def preprocess(snapshots, dataset):
    tg_snapshots = []
    node_mappings = []

    for graph in snapshots['graphs']:
        tg_snapshot, node_mapping = preprocess_graph_n2v_tg(graph)

        tg_snapshots.append(tg_snapshot)
        node_mappings.append(node_mapping)
        
    return {
        dataset: {
            'graphs': tg_snapshots,
            'node_mappings': node_mappings
        }
    }


def main():
    cfg_path = 'experiments/configs/convert_nx_graphs_to_tg_data.yaml'
    with open(cfg_path, 'r') as fin:
        cfg = yaml.safe_load(fin)

    dataset = sys.argv[1]

    snapshots = nx.read_gpickle(
        cfg['paths']['input'].replace('${dataset}', dataset)
    )[dataset]

    tg_snapshots = preprocess(snapshots, dataset)

    out_path = cfg['paths']['output'].replace('${dataset}', dataset)
    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    with open(out_path, 'wb') as f:
        pickle.dump(obj=tg_snapshots, file=f)


if __name__ == '__main__':
    main()
