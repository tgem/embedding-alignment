import argparse
import os
import yaml

import numpy as np
import pandas as pd
from tqdm import tqdm

from dynalign import aligners as alg
from dynalign.embeddings import EMBEDDING_AGGREGATION_MODELS
from dynalign.evaluation import compute_auc
from dynalign import regressors as reg
from dynalign import selectors as sel


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-d',
        '--dataset',
        help='Name of the dataset',
        type=str,
        required=True,
    )

    return parser.parse_args()


def get_aligners(cfg):
    if cfg['name'] == 'FILDNEPercent':
        assert 'percent' in cfg['parameters'].keys()
        param = cfg['parameters']['percent']

        return [
            {
                'params': {'percent': percent},

                'name': f'FILDNE_{str(round(percent, 1))}',
                'group': 'FILDNEPercent',
                'cls': alg.FILDNEPercentAligner,
                'cfg': {
                    'percent': percent,
                    'regressor': reg.OrthogonalProcrustesRegressor,
                    'reg_args': {}
                }
            }
            for percent in np.arange(param['min'], param['max'], param['step'])
        ]
    elif cfg['name'] == 'EdgeJaccardPercent':
        assert 'percent' in cfg['parameters'].keys()
        param = cfg['parameters']['percent']

        return [
            {
                'params': {'percent': percent},

                'name': f'EdgeJaccardPercent_{str(round(percent, 1))}',
                'group': 'EdgeJaccardPercent',
                'cls': alg.CustomDynAligner,
                'cfg': {
                    'nodes_selector': {
                        'cls': sel.EdgeJaccardNodesSelector,
                        'args': {
                            'selection_method': 'percent',
                            'selection_method_args': {
                                'percent': percent
                            }
                        }
                    },
                    'regressor': reg.OrthogonalProcrustesRegressor,
                    'reg_args': {}
                }
            }
            for percent in np.arange(param['min'], param['max'], param['step'])
        ]
    elif cfg['name'] == 'EmbeddingNeighborJaccardPercent':
        assert 'n' in cfg['parameters'].keys()
        assert 'percent' in cfg['parameters'].keys()
        p1 = cfg['parameters']['n']
        p2 = cfg['parameters']['percent']

        return [
            {
                'params': {'n': n, 'percent': percent},

                'name': f'EmbeddingNeighborJaccardPercent'
                f'_n_{str(round(n, 2))}'
                f'_{str(round(percent, 2))}',
                'group': 'EmbeddingNeighborJaccardPercent',
                'cls': alg.CustomDynAligner,
                'cfg': {
                    'nodes_selector': {
                        'cls': sel.EmbeddingNeighborJaccard,
                        'args': {
                            'neighbors_percent': n,
                            'selection_method': 'percent',
                            'selection_method_args': {
                                'percent': percent
                            }
                        },
                    },
                    'regressor': reg.OrthogonalProcrustesRegressor,
                    'reg_args': {}
                }
            }
            for n in np.arange(p1['min'], p1['max'], p1['step'])
            for percent in np.arange(p2['min'], p2['max'], p2['step'])
        ]
    elif cfg['name'] in [
        'TemporalCloseness',
        'TemporalBetweenness',
        'TemporalKShellScore',
        'TemporalDegreeDeviation'
    ]:
        assert 'percent' in cfg['parameters'].keys()
        fn_mapping = {
            'TemporalCloseness': 'closeness',
            'TemporalBetweenness': 'betweenness',
            'TemporalKShellScore': 'k_shell_score',
            'TemporalDegreeDeviation': 'degree_deviation'
        }
        param = cfg['parameters']['percent']
        name = cfg['name']
        return [
            {
                'params': {'percent': percent},

                'name': f'{name}_{str(round(percent, 1))}',
                'group': name,
                'cls': alg.TemporalCentralityMeasureAligner,
                'cfg': {
                    'percent': percent,
                    'regressor': reg.OrthogonalProcrustesRegressor,
                    'reg_args': {},
                    'centrality_measure_name': fn_mapping[name],
                    'verbose': False,
                }
            }
            for percent in np.arange(param['min'], param['max'], param['step'])
        ]
    else:
        raise RuntimeError('Unknown aligner:', cfg['name'])


def evaluate_aligner(all_graphs, all_embeddings, cache_path, aligner):
    all_aligner_results = alg.align_all(
        all_graphs=all_graphs,
        all_embeddings=all_embeddings,
        aligners=[aligner],
        cache_path=cache_path,
    )

    results = []

    for key in all_graphs.keys():
        lp_ds = all_graphs[key]['lp_ds'][-1]
        aligner_results = all_aligner_results[key]

        for idx, alg_res in tqdm(
            iterable=enumerate(aligner_results),
            desc='Evaluate aligned embeddings',
            total=len(aligner_results),
            leave=False,
        ):
            embeddings = alg_res['aligned_embedding']
            eam = EMBEDDING_AGGREGATION_MODELS['Average']()

            if all(embeddings):
                auc = compute_auc(
                    embeddings=embeddings,
                    embedding_agg_model=eam,
                    lp_ds=lp_ds,
                )
            else:
                auc = np.nan

            results.append({
                'key': key,
                'aligner_group': aligner['group'],
                'aligner_name': aligner['name'],
                'aligner_params': aligner['params'],
                'emb_run': idx,
                'auc': auc,
            })

    return results


def main():
    args = get_args()

    config_file = 'experiments/configs/first/aligner_grid_search.yaml'

    with open(config_file, 'r') as fin:
        cfg = yaml.safe_load(fin)

    dataset_name = args.dataset

    graphs = pd.read_pickle(
        cfg['paths']['input']['graphs'].replace('${NAME}', dataset_name)
    )
    embeddings = pd.read_pickle(
        cfg['paths']['input']['embeddings'].replace('${NAME}', dataset_name)
    )

    output_path = cfg['paths']['output'].replace('${NAME}', dataset_name)
    os.makedirs(os.path.dirname(output_path), exist_ok=True)

    results = []

    for aligner_cfg in tqdm(cfg['aligners'], desc='Aligner group'):
        cache_path = cfg['paths']['input'][
            'temporal_scores'
            if 'Temporal' in aligner_cfg['name']
            else 'scores'
        ].replace('${NAME}', dataset_name)

        aligners = get_aligners(cfg=aligner_cfg)
        for aligner in tqdm(aligners, desc='Aligner'):
            rs = evaluate_aligner(
                all_graphs=graphs,
                all_embeddings=embeddings,
                aligner=aligner,
                cache_path=cache_path,
            )
            results.extend(rs)

            # Save after every iteration in case something goes wrong
            pd.DataFrame.from_records(results).to_csv(
                path_or_buf=output_path,
                index=False,
            )


if __name__ == '__main__':
    main()
