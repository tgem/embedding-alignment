import argparse
import importlib
import os
import pickle

import pandas as pd
import yaml

from dynalign import aligners as alg


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-a',
        '--aligner',
        help='File with aligner configurations',
        type=str,
        required=True,
    )

    return parser.parse_args()


def load_aligner(file_path: str, dataset_name: str):
    module_path = file_path.replace('.py', '').replace('/', '.')
    aligners = getattr(importlib.import_module(module_path), 'ALIGNERS')

    if dataset_name not in aligners.keys():  # Procrustes or ProcrustesUnchanged
        return aligners
    else:
        return aligners[dataset_name]


def main():
    args = get_args()

    config_file = 'experiments/configs/first/align_embeddings.yaml'

    with open(config_file, 'r') as fin:
        cfg = yaml.safe_load(fin)

    all_datasets = cfg['datasets']
    graphs_path = cfg['paths']['input']['graphs']
    embeddings_path = cfg['paths']['input']['embeddings']
    aligned_path = cfg['paths']['output']

    for dataset in all_datasets:
        print(dataset)

        aligner = load_aligner(file_path=args.aligner, dataset_name=dataset)

        cache_path = cfg['paths']['input'][
            'temporal_scores_cache'
            if 'Temporal' in aligner['group']
            else 'scores_cache'
        ]

        # Prepare input data
        graphs = pd.read_pickle(graphs_path.replace('${dataset}', dataset))
        embeddings = pd.read_pickle(
            embeddings_path.replace('${dataset}', dataset)
        )

        # Perform alignment
        aligned_embeddings = alg.align_all(
            all_graphs=graphs,
            all_embeddings=embeddings,
            aligners=[aligner],
            cache_path=cache_path.replace('${dataset}', dataset),
        )

        # Save results
        output_path = (
            aligned_path
            .replace('${ALIGNER}', aligner['group'])
            .replace('${dataset}', dataset)
        )
        os.makedirs(os.path.dirname(output_path), exist_ok=True)
        with open(output_path, 'wb') as f:
            pickle.dump(obj=aligned_embeddings, file=f)


if __name__ == '__main__':
    main()
