import argparse
import math
import multiprocessing as mp
import os
import pickle

import numpy as np
import pandas as pd
import torch
import yaml
from tqdm import tqdm

from dynalign import embeddings as agg
from dynalign.graph_reconstruction import map_score_batch, distortion_batch

EMBEDDING_AGGREGATION_MODELS = {
    'Average': agg.EmbeddingAverageAggregation
}


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-a',
        '--aligner',
        help='Name of aligner',
        type=str,
        required=True,
    )

    return parser.parse_args()


def get_batch_indices(indices, batch_st_id, batch_en_id):
    batch_indices = [
        (it[0] - batch_st_id, it[1])
        for it in indices
        if batch_st_id <= it[0] < batch_en_id
    ]
    batch_indices = (
        [it[0] for it in batch_indices],
        [it[1] for it in batch_indices]
    )
    return batch_indices


def get_batch_ids(batch_id, batch_size, max_id):
    batch_st_idx = batch_id * batch_size
    batch_en_idx = (batch_id + 1) * batch_size
    if batch_en_idx >= max_id:
        batch_en_idx = max_id - 1

    return batch_st_idx, batch_en_idx


def get_min_max_from_emb_dists_batch(
    indices, nb_batches, batch_size, emb_mtx
):
    dist_min = []
    dist_max = []

    for i in tqdm(
        iterable=range(nb_batches),
        desc='Min/max calculatiion',
        leave=False
    ):
        batch_st_idx, batch_en_idx = get_batch_ids(
            batch_id=i,
            batch_size=batch_size,
            max_id=len(emb_mtx)
        )
        batch_indices = get_batch_indices(
            indices=indices,
            batch_st_id=batch_st_idx,
            batch_en_id=batch_en_idx
        )
        emb_dists_batch = torch.cdist(
            emb_mtx[batch_st_idx: batch_en_idx],
            emb_mtx,
            p=2
        )

        emb_dists_batch = emb_dists_batch[batch_indices]

        dist_min.append(torch.min(emb_dists_batch).item())
        dist_max.append(torch.max(emb_dists_batch).item())

    return np.min(dist_min), np.max(dist_max)


def calculate_gr_metrics_batched(graph, graph_dists, emb_mtx, batch_size):
    """Calculates graph reconstruction metrics."""

    nb_batches = math.ceil(len(emb_mtx) / batch_size)

    row, col, _ = graph_dists.coo()
    indices = set(zip(row.tolist(), col.tolist()))
    emb_dist_min, emb_dist_max = get_min_max_from_emb_dists_batch(
        indices=indices,
        nb_batches=nb_batches,
        batch_size=batch_size,
        emb_mtx=emb_mtx
    )

    mAP_batched = []
    distortion_batched = []

    for i in tqdm(
        iterable=range(nb_batches),
        desc='Calculating metrics batch',
        leave=False
    ):
        batch_st_idx, batch_en_idx = get_batch_ids(
            batch_id=i,
            batch_size=batch_size,
            max_id=len(emb_mtx)
        )

        emb_dists_batch = torch.cdist(
            emb_mtx[batch_st_idx:batch_en_idx],
            emb_mtx,
            p=2
        )

        emb_dists_batch = (
            (emb_dists_batch - emb_dist_min) /
            (emb_dist_max - emb_dist_min) *
            (graph_dists.max().numpy().item() - 1) + 1
        ).numpy()

        mAP_batched.append(
            map_score_batch(
                graph=graph,
                emb_dists=emb_dists_batch,
                batch_metadata={
                    'batch_nb_nodes': len(emb_dists_batch),
                    'batch_first_id': batch_st_idx,
                    'batch_en_id': batch_en_idx,
                    'batch_size': batch_size
                }
            )
        )
        distortion_batched.append(
            distortion_batch(
                graph_dists=(graph_dists[
                             batch_st_idx:batch_en_idx
                             ].to_dense().numpy()),
                emb_dists=emb_dists_batch,
            )
        )

    mAP = (
        np.sum([it[0] for it in mAP_batched]) /
        np.sum([it[1] for it in mAP_batched])
    )
    distortion = (
        np.sum([it[0] for it in distortion_batched]) /
        np.sum([it[1] for it in distortion_batched])
    )

    return mAP, distortion


def evaluate_gr_for_emb_run_batched(
    embedding,
    graphs_data_path,
    graph_data_key,
    agg_model_cls,
    batch_size=1024
):
    graph_data = pd.read_pickle(graphs_data_path)

    graph = graph_data[graph_data_key]['graph']
    sp = graph_data[graph_data_key]['sp']
    node_mapping = graph_data[graph_data_key]['mapping']
    mAP, distortion = np.nan, np.nan

    if all(embedding['embeddings']):
        emb = agg_model_cls().predict(embedding['embeddings'])
        emb = emb.to_dense(mapping=node_mapping)
        emb = torch.from_numpy(emb)

        mAP, distortion = calculate_gr_metrics_batched(
            graph=graph,
            graph_dists=sp,
            emb_mtx=emb,
            batch_size=batch_size
        )

    return {
        'run': embedding['run'],
        'aligner': embedding['aligner'],
        'group': embedding['group'],
        'mAP': mAP,
        'distortion': distortion,
    }


def evaluate_gr_emb_run_mp_wrapper(args):
    return evaluate_gr_for_emb_run_batched(**args)


def evaluate_gr(
    graphs_data_path, embeddings, batch_size, agg_model
):
    AGG_MODEL_CLS = EMBEDDING_AGGREGATION_MODELS[agg_model]
    results = {}

    graph_data_key = pd.read_pickle(graphs_data_path).keys()

    for key in tqdm(graph_data_key, desc='Dataset', leave=False):
        num_embeddings = len(embeddings[key])

        with mp.Pool(processes=num_embeddings) as pool:
            args = [
                {
                    'embedding': emb_run,
                    'graphs_data_path': graphs_data_path,
                    'graph_data_key': key,
                    'agg_model_cls': AGG_MODEL_CLS,
                    'batch_size': batch_size
                }
                for emb_run in embeddings[key]
            ]

            processed_batch = list(tqdm(
                iterable=pool.imap(evaluate_gr_emb_run_mp_wrapper, args),
                total=len(args),
                desc=f'Processing embeddings ds={key}'
            ))

        results[key] = processed_batch

    return results


def main():
    args = get_args()

    config_file = 'experiments/configs/first/evaluate_gr_avg.yaml'

    with open(config_file, 'r') as fin:
        cfg = yaml.safe_load(fin)

    aligner_group = args.aligner

    graphs_path = cfg['paths']['input']['sp']
    embeddings_path = cfg['paths']['input']['embeddings']
    aligned_emb_path = cfg['paths']['input']['aligned_embeddings']

    results_path = cfg['paths']['output'].replace('${ALIGNER}', aligner_group)

    gr_results = {}

    for dataset in cfg['datasets']:
        ds_graphs_path = graphs_path.replace('${dataset}', dataset)

        if aligner_group == 'None':
            embeddings = pd.read_pickle(
                embeddings_path.replace('${dataset}', dataset)
            )
            embeddings = {
                k: [
                    {
                        'run': int(ek.replace('embedding_', '')),
                        'aligner': 'None',
                        'group': aligner_group,
                        'embeddings': ev,
                    }
                    for ek, ev in v.items()
                ]
                for k, v in embeddings.items()
            }
        else:
            embeddings = pd.read_pickle(
                aligned_emb_path
                    .replace('${ALIGNER}', aligner_group)
                    .replace('${dataset}', dataset)
            )
            embeddings = {
                k: [
                    {**vals, 'embeddings': vals['aligned_embedding']}
                    for vals in v
                ]
                for k, v in embeddings.items()
            }

        res = evaluate_gr(
            graphs_data_path=ds_graphs_path,
            embeddings=embeddings,
            batch_size=cfg['batch_size'],
            agg_model=cfg['model'],
        )
        gr_results.update(res)

    os.makedirs(os.path.dirname(results_path), exist_ok=True)
    with open(results_path, 'wb') as f:
        pickle.dump(obj=gr_results, file=f)


if __name__ == '__main__':
    main()
