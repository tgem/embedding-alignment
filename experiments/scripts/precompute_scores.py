import argparse
import os
import pickle

import networkx as nx
import numpy as np
import pandas as pd
import yaml
from tqdm import tqdm

from dynalign.selectors import (
    EdgeJaccardNodesSelector,
    EmbeddingNeighborJaccard,
)


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-d',
        '--dataset',
        help='Name of the dataset',
        type=str,
        required=True,
    )

    return parser.parse_args()


def get_scores(
    selector_name: str,
    selector_args: dict,
    g_u: nx.Graph, g_v: nx.Graph,
    embeddings: dict,
    idx: int,
) -> dict:
    if selector_name == 'EdgeJaccardNodesSelector':
        return EdgeJaccardNodesSelector().get_scores(
            u=None, v=None,  # Not used in this class
            g_u=g_u, g_v=g_v,
        )
    elif selector_name == 'EmbeddingNeighborJaccard':
        all_neighbors_percents = np.arange(
            start=selector_args['neighbors_percent']['start'],
            stop=selector_args['neighbors_percent']['end'],
            step=selector_args['neighbors_percent']['step'],
        )
        all_scores = {}

        for emb_key, embs in embeddings.items():
            u, _ = embs[0]
            v, _ = embs[idx]

            all_scores[emb_key] = {
                nep: EmbeddingNeighborJaccard(neighbors_percent=nep).get_scores(
                    u=u, v=v,
                    g_u=g_u, g_v=g_v,
                )
                for nep in all_neighbors_percents
            }

        return all_scores
    else:
        raise ValueError(f'Unknown selector: {selector_name}')


def main():
    args = get_args()

    config_file = 'experiments/configs/first/precompute_scores.yaml'

    with open(config_file, 'r') as fin:
        cfg = yaml.safe_load(fin)

    graphs_path = cfg['paths']['input']['graphs'].replace(
        '${dataset}', args.dataset
    )
    embeddings_path = cfg['paths']['input']['embeddings'].replace(
        '${dataset}', args.dataset
    )
    cached_path = cfg['paths']['output'].replace('${dataset}', args.dataset)

    # Read graphs
    datasets = pd.read_pickle(graphs_path)
    embeddings = pd.read_pickle(embeddings_path)

    all_scores = {}

    pbar = tqdm(iterable=cfg['selectors'].items(), desc='Selector')

    for selector_name, selector_args in pbar:
        pbar.set_description(f'Selector: {selector_name}')

        all_scores[selector_name] = {}

        for dataset_id in tqdm(datasets.keys(), desc='Dataset', leave=False):
            values = []
            graphs = datasets[dataset_id]['graphs']

            for idx in tqdm(
                iterable=range(1, len(graphs)),
                desc='Graph',
                leave=False,
            ):
                g_u = graphs[0]
                g_v = graphs[idx]

                scores = get_scores(
                    selector_name=selector_name,
                    selector_args=selector_args,
                    g_u=g_u, g_v=g_v,
                    embeddings=embeddings[dataset_id],
                    idx=idx,
                )

                values.append(scores)

            all_scores[selector_name][dataset_id] = values

    pbar.close()

    # Save results
    os.makedirs(os.path.dirname(cached_path), exist_ok=True)
    with open(cached_path, 'wb') as f:
        pickle.dump(obj=all_scores, file=f)


if __name__ == '__main__':
    main()
