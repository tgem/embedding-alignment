import os
import pickle
import sys
from typing import Tuple

import networkx as nx
import numpy as np
import pandas as pd
import torch
import torch_sparse as ts
import yaml
from tqdm import tqdm

from dynalign import preprocess as dpp
from dynalign import temporal_scores as dts


def convert_sp_dict_to_sparse_mtx(
    input_dict, remove_diag=False, mapping=None
):
    """Converts nodes shortest paths to sparse matrix form."""
    nb_nodes = len(mapping)
    rows = []
    cols = []
    values = []

    for node_1 in input_dict.keys():
        for node_2, value in input_dict[node_1].items():
            node_1_id = mapping[node_1] if mapping else node_1
            node_2_id = mapping[node_2] if mapping else node_2

            rows.append(node_1_id)
            cols.append(node_2_id)
            values.append(value)

    st = ts.SparseTensor(
        row=torch.tensor(rows),
        col=torch.tensor(cols),
        value=torch.tensor(values),
        sparse_sizes=(nb_nodes, nb_nodes)

    )
    if remove_diag:
        st = ts.remove_diag(st)

    return st


def calculate_sp(
    dataset_name: str, datasets: dict
) -> Tuple[dict, dict]:
    sp = {}
    graphs = {}
    for key in tqdm(datasets.keys(), desc='Datasets'):
        graph = dpp.combine_graphs(
            dataset_name=dataset_name,
            graphs=datasets[key]['graphs'],
        )
        _, tsp_dists = dts.compute_temporal_shortest_paths(
            temporal_network=graph,
            max_delta=1,
        )
        tsp_dists = {k: dict(v) for k, v in tsp_dists.items()}
        sp[key] = tsp_dists
        graphs[key] = dts.to_networkx(
            graph=graph,
            create_using=type(datasets[key]['graphs'][0]),
        )

    return sp, graphs


def preprocess_data(graph, sp):
    mapping = dict(zip(
        list(graph.nodes()),
        np.arange(0, graph.number_of_nodes())
    ))

    if nx.is_directed(graph):
        graph = nx.DiGraph(graph)
    else:
        graph = nx.Graph(graph)

    graph = nx.relabel_nodes(graph, mapping)
    graph_dists = convert_sp_dict_to_sparse_mtx(
        input_dict=sp,
        mapping=mapping,
        remove_diag=True
    )
    return graph, graph_dists, mapping


def main():
    # Read config
    with open('experiments/configs/calculate_sp.yaml', 'r') as fin:
        cfg = yaml.safe_load(fin)

    dataset_name = sys.argv[1]

    # Read data
    datasets = pd.read_pickle(
        cfg['paths']['input'].replace('${dataset}', dataset_name)
    )

    shortest_paths, graphs = calculate_sp(
        dataset_name=dataset_name, datasets=datasets
    )

    graph_data = {}
    for key in graphs.keys():
        graph, sp, mapping = preprocess_data(
            graph=graphs[key],
            sp={
                int(k): {int(kk): vv for kk, vv in v.items()}
                for k, v in shortest_paths[key].items()
            },
        )

        graph_data[key] = {
            'graph': graph,
            'sp': sp,
            'mapping': mapping
        }

    # Save results
    output_path = cfg['paths']['output'].replace('${dataset}', dataset_name)
    os.makedirs(os.path.dirname(output_path), exist_ok=True)
    with open(output_path, 'wb') as f:
        pickle.dump(obj=graph_data, file=f)


if __name__ == '__main__':
    main()