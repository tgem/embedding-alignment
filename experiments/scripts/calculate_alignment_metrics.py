import os
import pickle

import numpy as np
import pandas as pd
from tqdm.auto import tqdm

from dynalign.metrics import ped_score, ssd_score, rnd_score


def load(dataset: str, aligner: str):
    emb = pd.read_pickle(f'data/embeddings/{dataset}.pkl')[dataset]
    aligned = pd.read_pickle(f'data/aligned/first/{aligner}/{dataset}.pkl')[
        dataset
    ]

    return emb, aligned


def compute(dataset: str, aligner: str, verbose: bool = False):
    emb, aligned = load(dataset, aligner)

    values = {'ped': [], 'ssd': [], 'rnd': [], 'rnd_unaligned': []}

    for ae in tqdm(aligned, desc='Run'):
        run = ae['run']

        embeddings = [e for e, _ in emb[f'embedding_{run}'][1:]]
        aligned_embeddings = ae['aligned_embedding'][1:]
        reference_nodes = ae['reference_nodes']

        if not all(aligned_embeddings):
            snapshots_ped = np.nan
            snapshots_ssd = np.nan
            snapshots_rnd = np.nan
            snapshots_rnd_unaligned = np.nan
        else:
            snapshots_ped = 0
            snapshots_ssd = 0
            snapshots_rnd = 0
            snapshots_rnd_unaligned = 0

            for embedding, aligned_embedding, rn in tqdm(
                    iterable=zip(embeddings, aligned_embeddings,
                                 reference_nodes),
                    desc='Snapshots',
                    total=len(embeddings),
                    leave=False,
                    disable=not verbose,
            ):
                rn = [str(it) for it in rn]

                snapshots_ped += ped_score(
                    embedding=embedding,
                    aligned_embedding=aligned_embedding,
                    batch_size=2_000,
                    verbose=verbose,
                )
                snapshots_ssd += ssd_score(
                    embedding=embedding,
                    aligned_embedding=aligned_embedding,
                    reference_nodes=rn,
                    batch_size=2_000,
                    verbose=verbose,
                )
                snapshots_rnd += rnd_score(
                    reference_embedding=emb[f'embedding_{run}'][0][0],
                    aligned_embedding=aligned_embedding,
                    reference_nodes=rn,
                    verbose=verbose,
                )
                snapshots_rnd_unaligned += rnd_score(
                    reference_embedding=emb[f'embedding_{run}'][0][0],
                    aligned_embedding=embedding,
                    reference_nodes=rn,
                    verbose=verbose,
                )

            snapshots_ped /= len(embeddings)
            snapshots_ssd /= len(embeddings)
            snapshots_rnd /= len(embeddings)
            snapshots_rnd_unaligned /= len(embeddings)

        values['ped'].append(snapshots_ped)
        values['ssd'].append(snapshots_ssd)
        values['rnd'].append(snapshots_rnd)
        values['rnd_unaligned'].append(snapshots_rnd_unaligned)

    return values


def main():
    DATASETS = [
        'ia-hypertext', 'ia-enron-employees', 'ia-radoslaw-email',
        'fb-forum', 'fb-messages', 'bitcoin-alpha',
        'bitcoin-otc', 'ppi', 'ogbl-collab'
    ]
    ALIGNERS = [
        'Procrustes', 'ProcrustesUnchanged', 'FILDNEPercent',
        'EdgeJaccardPercent', 'EmbeddingNeighborJaccardPercent',
        'TemporalBetweenness',
        'TemporalCloseness', 'TemporalKShellScore', 'TemporalDegreeDeviation',
    ]

    output_path = 'data/metrics/alignment_metrics.pkl'
    all_values = {}
    for dataset in DATASETS:
        for aligner in ALIGNERS:
            print(dataset, aligner)
            all_values[(dataset, aligner)] = compute(
                dataset=dataset, aligner=aligner
            )
            os.makedirs(os.path.dirname(output_path), exist_ok=True)
            with open(output_path, 'wb') as f:
                pickle.dump(obj=all_values, file=f)


if __name__ == '__main__':
    main()
