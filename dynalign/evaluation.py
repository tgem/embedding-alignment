import itertools
import math
import warnings
from collections import OrderedDict
from copy import deepcopy
from typing import List

import networkx as nx
import numpy as np
from sklearn import linear_model as sk_lm, utils as sk_utils
from sklearn import metrics

from dynalign import embeddings as agg
from dynalign.embeddings import KeyedModel


def hadamard_op(u, v):
    """Calculates hadamard edge representation between two node vectors."""
    return u * v


def calculate_edge_embedding(keyed_model, edges, op):
    """Calculates edge embedding."""
    representations = []

    for u, v in edges:
        e_u = keyed_model.get_vector(str(u))
        e_v = keyed_model.get_vector(str(v))

        representations.append(op(e_u, e_v))

    return representations


def compute_auc(
    embeddings: List[KeyedModel],
    embedding_agg_model: agg.EmbeddingAggregation,
    lp_ds
) -> float:
    emb = embedding_agg_model.predict(embeddings)

    lrm = LogisticRegressionModel(embedding=emb)
    lrm.fit(x=lp_ds.x_train, y=lp_ds.y_train)

    return lrm.validate(x=lp_ds.x_test, y=lp_ds.y_test).as_dict()['auc']


def check_arr_is_ndarray(arr, arr_name):
    """Checks wheter given arr is np.ndarray.

    :param arr: Input array
    :param arr_name: array name
    :type arr_name: str
    :raise: ValueError: Whether check is failed
    """
    if not isinstance(arr, np.ndarray):
        raise ValueError(
            f'{arr_name} is not np.ndarray'
        )


def split_list(input_list, split_ratio):
    """Splits list into two chunks.

    :param input_list: Input list
    :type input_list: list
    :param split_ratio: Split ratio of list
    :type split_ratio: float
    :return: Two chunks of input_list
    :rtype (list, list)
    """
    margin_item = math.ceil(len(input_list) * split_ratio)

    left = input_list[:margin_item]
    right = input_list[margin_item:]
    return left, right


def split_edges(edges, split_ratio=0.5, data=False):
    """Splits list of edges into two slices.

    :param edges: List of edges
    :type edges: list
    :param split_ratio: Ratio of list split (default: 0.5)
    :type split_ratio: float
    :param data: Denotes whether edge data will be copied
    :type data: bool
    :return: Two list slices
    :rtype: (list, list)
    """
    left, right = split_list(edges, split_ratio)
    if not data:
        left = [(edge[0], edge[1]) for edge in left]
        right = [(edge[0], edge[1]) for edge in right]
    return left, right


class ROCMetric:
    """ROC Metric utilities.

    Provides statistics: (tpr_percentile, fpr_percentile)
    """

    def __init__(self, y_true, y_score):
        """Inits class.

        :param y_true: True labels
        :param y_score: Scores
        """
        y_true = np.array(y_true)
        y_score = np.array(y_score)

        self.fpr, self.tpr, self.thresholds = metrics.roc_curve(
            y_true=y_true,
            y_score=y_score
        )

    def get_tpr_interpolation(self, x):
        """Returns tpr of unknown fpr x value based on linear interpolation.

        :param x: Input unknown value of point x (range 0-1)
        :type x: float
        :return: Interpolated value
        :rtype: float
        """
        return np.interp(x, self.fpr, self.tpr)

    def get_fpr_interpolation(self, x):
        """Returns fpr of unknown tpr x value based on linear interpolation.

        :param x: Input unknown value of point x (range 0-1)
        :type x: float
        :return: Interpolated value
        :rtype: float
        """
        return np.interp(x, self.tpr, self.fpr)

    def get_tpr_percentile(self, percentile):
        """Returns tpr ratio of given percentile.

        :param percentile: Input percentile (range 0-100)
        :type percentile: float
        :return: TPR ratio of given percentile
        :rtype: float
        """
        return np.percentile(a=self.tpr, q=percentile)

    def get_fpr_percentile(self, percentile):
        """Returns fpr ratio of given percentile.

        :param percentile: Input percentile (range 0-100)
        :type percentile: float
        :return: FPR ratio of given percentile
        :rtype: float
        """
        return np.percentile(a=self.fpr, q=percentile)


class ClassificationReport:
    """Class that provides various classification metrics.

    Provided statistics: (precision, recall, f1-score, accuracy, auc)
    """

    def __init__(self, y_pred, y_true, y_score=None):
        """Inits class and calculates metrics.

        Only if y_score is given, then AUC metric will be calculated.
        :param y_true: Ground truth labels
        :type y_true: np.ndarray
        :param y_pred: Predicted labels
        :type y_pred: np.ndarray
        :param y_score: Predicted probabilities or scores (default: None)
        :type y_score: np.ndarray
        """
        check_arr_is_ndarray(y_pred, 'y_pred')
        check_arr_is_ndarray(y_true, 'y_true')
        if y_score is not None:
            check_arr_is_ndarray(y_score, 'y_score')

        self.labels = np.unique(y_true)
        self.item_count = len(y_true)

        self.nb_classes = len(self.labels)
        self.cm = metrics.confusion_matrix(
            y_true=y_true,
            y_pred=y_pred,
        )

        self._metrics_dict = self._generate_report(
            y_true=y_true, y_pred=y_pred, y_score=y_score
        )

    def _generate_report(self, y_true, y_pred, y_score):
        """Calculates metrics."""
        prfs = metrics.precision_recall_fscore_support(
            y_true=y_true, y_pred=y_pred, labels=self.labels
        )

        if self.cm.shape == (1, 1):
            accuracy_per_class = [1]
        else:
            accuracy_per_class = np.array([
                self.cm[label_id][label_id] / np.sum(self.cm[label_id])
                for label_id in self.labels
            ])

        calculated_metrics = OrderedDict([
            ('accuracy', accuracy_per_class),
            ('precision', prfs[0]),
            ('recall', prfs[1]),
            ('f1-score', prfs[2]),
            ('support', prfs[3])
        ])

        metrics_dict = {
            class_id: OrderedDict([
                (metric_nm, calculated_metrics[metric_nm][class_id])
                for metric_nm in calculated_metrics.keys()
            ])
            for class_id in range(self.nb_classes)
        }

        if y_score is not None:
            if self.nb_classes == 2:
                auc = metrics.roc_auc_score(y_true=y_true, y_score=y_score)
            else:
                warnings.warn(
                    'AUC is defined only for two classes scenario!',
                    RuntimeWarning
                )
                auc = None

            metrics_dict['auc'] = auc

        # Calculate averages
        metrics_dict['micro'] = self._calculate_micro_metrics(
            y_true=y_true, y_pred=y_pred
        )

        metrics_dict['macro'] = {
            metric_nm: np.mean([
                metrics_dict[class_id][metric_nm]
                for class_id in range(self.nb_classes)
            ])
            for metric_nm in calculated_metrics.keys() if metric_nm != 'support'
        }

        metrics_dict['weighted'] = {
            metric_nm: np.sum([
                metrics_dict[class_id][metric_nm] * (
                        metrics_dict[class_id]['support'] / self.item_count
                )
                for class_id in range(self.nb_classes)
            ])
            for metric_nm in calculated_metrics.keys() if metric_nm != 'support'
        }

        return metrics_dict

    def _calculate_micro_metrics(self, y_true, y_pred):
        """Calculated micro averaged metrics."""
        accuracy = metrics.accuracy_score(y_true=y_true, y_pred=y_pred)
        prfs = metrics.precision_recall_fscore_support(
            y_true=y_true, y_pred=y_pred, labels=self.labels, average='micro'
        )
        micro = OrderedDict([
            ('accuracy', accuracy),
            ('precision', prfs[0]),
            ('recall', prfs[1]),
            ('f1-score', prfs[2]),
        ])
        return micro

    def as_dict(self):
        """Returns dictionary with metrics.

        :return: Metrics dictionary
        :rtype: dict
        """
        return self._metrics_dict

    def as_flat_dict(self):
        """Presents all metrics a flat dict (metrics per class + avg).

        :return: Flattened metrics dict
        :rtype: dict
        """
        flat_dict = {}

        for key, keyed_metrics in self._metrics_dict.items():
            if isinstance(keyed_metrics, dict):
                for mt_name, mt_value in keyed_metrics.items():
                    flat_dict[f'{key}_{mt_name}'] = mt_value.item()
            elif isinstance(keyed_metrics, float):
                flat_dict[key] = keyed_metrics.item()

        return flat_dict

    def __str__(self):
        """Creates string representation of ClassificationReport object."""
        rp = ClassificationReportPrinter(
            nb_classes=self.nb_classes,
            labels=self.labels,
            metrics_dict=self._metrics_dict)

        return rp.as_str()


class ClassificationReportPrinter:
    """Prints report in eye friendly readable format.

    Styled similarly as sklearn.metrics.classification_report.
    """

    def __init__(self, nb_classes, labels, metrics_dict):
        """Inits the class.

        :param nb_classes: Number of classes
        :type nb_classes: int
        :param labels: Class labels
        :type labels: np.ndarray
        :param metrics_dict: Input metrics
        :type metrics_dict: dict
        """
        self._metrics = metrics_dict
        self.nb_classes = nb_classes
        self.labels = labels
        self.display_metrics = [
            'accuracy',
            'precision',
            'recall',
            'f1-score',
            'support'
        ]

    def _write_avg_metrics(self, avg_name):
        """Writes metrics average.

        :param avg_name: Averaging name e.g. micro
        :type avg_name: str
        :return: String with averages
        :rtype: str
        """
        missing_metric_str = '-'

        micro_avg_header = f'{avg_name} avg'
        avg_str = f'{micro_avg_header:>15} '

        if self.nb_classes > 1:
            for m_name in self.display_metrics:
                if m_name != 'support':
                    avg_str += f'{self._metrics[avg_name][m_name]:>10.2f} '
                else:
                    avg_str += f'{missing_metric_str:>10}'
        else:
            for _ in range(len(self.display_metrics)):
                avg_str += f'{missing_metric_str:>10} '

        avg_str += '\n'
        return avg_str

    def as_str(self):
        """Presents all metrics as string."""
        # Write header
        header_name = 'class'
        header = f'{header_name:>15} '
        for metric_name in self.display_metrics:
            header += f'{metric_name:>10} '
        header += '\n'

        # Write metrics body
        body = ''
        for i in range(self.nb_classes):
            # Write class name
            body += f'{self.labels[i]:>15} '
            for metric_name in self.display_metrics:
                body += f'{self._metrics[i][metric_name]:>10.2f} '
            body += '\n'
        body += '\n'

        # Write metrics averages

        # Micro average
        micro_avg = self._write_avg_metrics(avg_name='micro')
        macro_avg = self._write_avg_metrics(avg_name='macro')
        weighted_avg = self._write_avg_metrics(avg_name='weighted')

        auc_str = ''
        # auc
        if 'auc' in self._metrics.keys():
            auc_header = 'AUC'
            auc_score = self._metrics['auc']
            auc_str = f'\n{auc_header:>15} '
            if auc_score is not None:
                auc_str += f'{auc_score:>10.2f} '
            else:
                auc_str += 'undefined'

        return header + body + micro_avg + macro_avg + weighted_avg + auc_str


class LogisticRegressionModel:
    """Edge Classification model based on logistic regression."""

    def __init__(self, embedding, args=None, operator=hadamard_op):
        """Inits model with embedding data.
        """
        args = args if args else {}

        self._clf = sk_lm.LogisticRegression(solver='liblinear', **args)
        self._ee_op = operator
        self._emb = embedding

    def fit(self, x, y):
        """Fits model with data.

        :param x: Input edges
        :type x: np.ndarray
        :param y: Input labels
        :type y: np.ndarray
        """
        self._clf.fit(X=self._to_edge_emb(x), y=y.transpose()[0])

    def predict(self, x):
        """Predicts the existence of given edges.

        :param x: Input edges
        :type x: np.ndarray
        :return: Predictions
        :rtype: np.ndarray
        """
        return self._clf.predict(self._to_edge_emb(x))

    def predict_proba(self, x):
        """Predicts scores.

        :param x: Input edges
        :type x: np.ndarray
        :return: Predictions
        :rtype: np.ndarray
        """
        return self._clf.predict_proba(self._to_edge_emb(x))

    def _to_edge_emb(self, x, ):
        """Returns embedding of the edge.

        :param x: Input edges
        :rtype x: list
        :return: List of edge embeddings
        :rtype: np.ndarray
        """
        return np.array(calculate_edge_embedding(
            self._emb, x, self._ee_op
        ))

    def validate(self, x, y):
        """Validates data and returns classification report.

        :param x: Input edges
        :type x: np.ndarray
        :param y: Input labels
        :type y: np.ndarray
        :return: Classification metrics
        :rtype ClassificationReport
        """
        return ClassificationReport(
            y_true=y,
            y_pred=self.predict(x),
            y_score=self.predict_proba(x)[:, 1]
        )


class LinkPredictionDataset:
    def __init__(self):
        self.x_train = None
        self.y_train = None
        self.x_test = None
        self.y_test = None

    def _sample_negative_edges(self, nodes, graph, nb_samples):
        """Samples number of non-existing edges from given graph."""
        u = deepcopy(list(nodes))
        v = deepcopy(list(nodes))
        np.random.shuffle(u)
        np.random.shuffle(v)

        neg_edges = []
        if not nb_samples:
            warnings.warn('nb_samples is equal to 0', RuntimeWarning)
            return []
        for edge in itertools.product(u, v):
            # Do not include existing edges
            if graph.has_edge(edge[0], edge[1]):
                continue

            # For directed graphs, when using Hadamard operator, we MUST NOT
            # include an edge if the reversed one exists in the graph, because it
            # would be noise for the classification model.
            # TODO: if case we change Hadamard operator this must be fixed
            if (isinstance(graph, (nx.DiGraph, nx.MultiDiGraph)) and
                    graph.has_edge(edge[1], edge[0])):
                continue

            neg_edges.append(edge)
            if len(neg_edges) == nb_samples:
                break

        return neg_edges

    def mk_link_prediction_dataset(
            self, graph, split_proportion, prev_nodes=None
    ):
        """Returns link prediction dataset from graph."""

        def post_process(x_pos, x_neg):
            x_np = np.concatenate((np.array(x_pos), np.array(x_neg)))
            y_pos = np.ones((len(x_pos), 1), dtype=int)
            y_neg = np.zeros((len(x_neg), 1), dtype=int)
            y_np = np.concatenate((y_pos, y_neg))

            x, y = sk_utils.shuffle(x_np, y_np, random_state=0)
            return x, y

        def filter_edge(edge):
            return edge[0] in prev_nodes and edge[1] in prev_nodes

        edges = list(graph.edges())
        if prev_nodes:
            edges = [it for it in edges if filter_edge(it)]
            av_nodes = set(graph.nodes()).intersection(set(prev_nodes))
        else:
            av_nodes = set(graph.nodes())

        train_data, test_data = split_edges(edges, split_proportion)
        neg_train_data = self._sample_negative_edges(
            nodes=av_nodes,
            graph=graph,
            nb_samples=len(train_data)
        )
        if not neg_train_data:
            warnings.warn('There is no neg_train_data', RuntimeWarning)
            return
        neg_test_data = self._sample_negative_edges(
            nodes=av_nodes,
            graph=graph,
            nb_samples=len(test_data)
        )
        if not neg_test_data:
            warnings.warn('There is no neg_test_data', RuntimeWarning)
            return

        x_train, y_train = post_process(train_data, neg_train_data)
        x_test, y_test = post_process(test_data, neg_test_data)

        self.x_train = x_train
        self.y_train = y_train
        self.x_test = x_test
        self.y_test = y_test

        return self
