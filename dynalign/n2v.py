import time

import torch
import torch_geometric
from torch_geometric.nn import Node2Vec
from tqdm import tqdm

from dynalign.embeddings import KeyedModel
from dynalign.tg_utils import preprocess_graph_n2v_tg


class Node2VecEmbedding:
    """Class that wraps Node2Vec method."""

    def __init__(
        self, dimensions, walk_length, nb_walks_per_node,
        p, q, batch_size, lr, w2v_epochs, context_size,
        node_index_mapping=None, log_steps=1, quiet=False
    ):
        """Inits the Node2VecEmbedding class.

        :param dimensions: Embedding dimension
        :type dimensions: int
        :param walk_length: Length of single random walk sequence
        :type walk_length: int
        :param nb_walks_per_node: Number of random walk sequences starting
                                  from each node
        :type nb_walks_per_node: int
        :param p: Parameter p. Controls probability of revisiting the node
                               in a walk
        :type p: float
        :param q: Parameter q. Controls probability of visiting nodes that
                               are further away from previous node
        :type q: float
        """
        # w2v epochs has to be renamed to iter with gensim < 4.0.0
        self._model_args = dict(
            embedding_dim=dimensions,
            walk_length=walk_length,
            walks_per_node=nb_walks_per_node,
            p=p,
            q=q,
            sparse=True,
            context_size=context_size  # temporarily hardcoded
        )

        self._learning_args = dict(
            batch_size=batch_size,
            lr=lr,
            epochs=w2v_epochs,
            log_steps=log_steps
        )
        self.quiet = quiet
        self.graph = None
        self.n2v_model = None
        self.loader = None
        self.optimizer = None
        self.data = None
        self.node_index_mapping = node_index_mapping
        self.device = f'cuda:0' if torch.cuda.is_available() else 'cpu'

    def _fit(self):
        self.n2v_model.train()
        for epoch in tqdm(
                range(1, self._learning_args['epochs'] + 1), disable=self.quiet
        ):
            for i, (pos_rw, neg_rw) in enumerate(self.loader):
                self.optimizer.zero_grad()
                loss = self.n2v_model.loss(
                    pos_rw.to(self.device),
                    neg_rw.to(self.device)
                )
                loss.backward()
                self.optimizer.step()

                if not self.quiet and (
                        (i + 1) % self._learning_args['log_steps'] == 0
                ):
                    print(
                        f'Epoch: {epoch:02d}, '
                        f'Step: {i + 1:03d}/{len(self.loader)}, '
                        f'Loss: {loss:.4f}'
                    )

    def init(self):
        self.n2v_model = Node2Vec(
            self.data.edge_index, **self._model_args
        ).to(self.device)
        self.loader = self.n2v_model.loader(
            batch_size=self._learning_args['batch_size'], shuffle=True
        )
        self.optimizer = torch.optim.SparseAdam(
            list(self.n2v_model.parameters()), lr=self._learning_args['lr']
        )

    def free_resources(self):
        del self.n2v_model
        del self.loader
        if 'cuda' in self.device:
            torch.cuda.empty_cache()

    def save(self):
        embeddings = self.n2v_model.embedding.weight.data.cpu().numpy()
        self.free_resources()

        emb_dict = {}
        for node_id, node_emb in enumerate(embeddings):
            if self.node_index_mapping:
                node_id = self.node_index_mapping[node_id]

            emb_dict[str(node_id)] = node_emb

        return KeyedModel(
            size=embeddings.shape[1],
            node_emb_vectors=emb_dict,
            fill_unknown_nodes=False,
        )

    def embed(self, graph):
        """Generates random walks over the given graph."""
        if not isinstance(graph, torch_geometric.data.Data):
            data, node_index_mapping = preprocess_graph_n2v_tg(graph)
            self.data = data
            self.node_index_mapping = node_index_mapping
        else:
            self.data = graph
        start_time = time.time()
        self.init()
        self._fit()
        end_time = time.time()

        calculation_time = end_time - start_time
        return self.save(), calculation_time
