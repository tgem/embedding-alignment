from abc import ABC, abstractmethod
import pickle
from timeit import default_timer as timer
from typing import Dict, Type, Optional

import numpy as np
from tqdm import tqdm

from dynalign.regressors import OrthogonalProcrustesRegressor
from dynalign.embeddings import KeyedModel


class AbstractAligner(ABC):

    def __init__(
        self,
        regressor: Type[OrthogonalProcrustesRegressor],
        reg_args: dict,
        cache: Optional[dict],
    ):
        self.regressor = regressor(**reg_args)

    @abstractmethod
    def align(self, u, v, g_u, g_v, **kwargs):
        pass


class AbstractDynAligner(AbstractAligner):

    def __init__(
        self,
        regressor: Type[OrthogonalProcrustesRegressor],
        reg_args: dict,
        cache: Optional[dict],
    ):
        super().__init__(
            regressor=regressor,
            reg_args=reg_args,
            cache=cache,
        )

    @abstractmethod
    def get_activity(self, g_u, nodes):
        pass

    @abstractmethod
    def get_scores(self, a_u, a_v):
        pass

    @abstractmethod
    def get_ref_nodes(self, g_u, g_v):
        pass


class BaseAligner(AbstractAligner):
    def __init__(
        self,
        regressor: Type[OrthogonalProcrustesRegressor],
        reg_args: dict,
        cache: Optional[dict],
    ):
        super().__init__(
            regressor=regressor,
            reg_args=reg_args,
            cache=cache,
        )

    def align(self, u, v, g_u, g_v):
        common_nodes = get_common_nodes(g_u, g_v)
        scores = dict(
            zip(common_nodes, np.zeros(len(common_nodes)))
        )
        u_train = u.to_numpy(common_nodes)
        v_train = v.to_numpy(common_nodes)
        self.regressor.fit(v_train, u_train)
        return self.predict(v), common_nodes, scores

    def predict(self, v):
        emb_shape = v.shape[1]
        v_nodes = v.nodes
        v = v.to_numpy(v_nodes)
        emb = self.regressor.predict(v)
        return KeyedModel(
            size=emb_shape,
            node_emb_vectors=dict(zip(v_nodes, emb)),
            fill_unknown_nodes=False,
        )


class FILDNEPercentAligner(AbstractDynAligner, BaseAligner):

    def __init__(
        self,
        percent: float,
        regressor: Type[OrthogonalProcrustesRegressor],
        reg_args: dict,
        cache: Optional[dict],
    ):
        super().__init__(
            regressor=regressor,
            reg_args=reg_args,
            cache=cache,
        )
        self.percent = percent

    def get_scores(self, a_u, a_v):
        return (
            np.abs(a_u - a_v) *
            (np.pi / 2 - np.arctan(np.maximum(a_u - a_v, a_v)))
        )

    def get_activity(self, g_u, nodes):
        degrees = []
        for node in nodes:
            degrees.append(len(g_u.edges(node)))
        return np.array(degrees)

    def get_nb_ref_nodes(self, num_nodes):
        return int(num_nodes * self.percent)

    def get_ref_nodes(self, g_u, g_v):
        nodes = [int(it) for it in get_common_nodes(g_u, g_v)]
        a_u = self.get_activity(g_u, nodes)
        a_v = self.get_activity(g_v, nodes)

        scores = self.get_scores(a_u, a_v)

        scores_dict = dict(zip(nodes, scores))
        nb_ref_nodes = self.get_nb_ref_nodes(len(nodes))
        reference_nodes = sorted(scores_dict.keys(), key=scores_dict.get)
        reference_nodes = reference_nodes[:int(nb_ref_nodes)]
        reference_nodes = [str(node) for node in reference_nodes]
        return reference_nodes, scores_dict

    def align(self, u, v, g_u, g_v):
        ref_nodes, scores = self.get_ref_nodes(g_u, g_v)
        u_train = u.to_numpy(ref_nodes)
        v_train = v.to_numpy(ref_nodes)
        self.regressor.fit(v_train, u_train)
        return self.predict(v), ref_nodes, scores


class DynAligner(BaseAligner):
    def __init__(
        self,
        regressor: Type[OrthogonalProcrustesRegressor],
        reg_args: dict,
        cache: Optional[dict],
    ):
        super().__init__(
            regressor=regressor,
            reg_args=reg_args,
            cache=cache,
        )

    def align(self, u, v, g_u, g_v):
        unchanged_nodes, changed_nodes = get_unchanged_changed_nodes(
            src_graph=g_u,
            changed_graph=g_v
        )
        u_train = u.to_numpy(unchanged_nodes)
        v_train = v.to_numpy(unchanged_nodes)
        self.regressor.fit(v_train, u_train)
        scores = dict(zip(unchanged_nodes, np.zeros(len(unchanged_nodes))))
        scores = {
            **scores,
            **(
                dict(zip(changed_nodes, np.ones(len(changed_nodes))))
            )
        }
        return self.predict(v), unchanged_nodes, scores


class CustomDynAligner(BaseAligner):
    def __init__(
        self,
        regressor: Type[OrthogonalProcrustesRegressor],
        reg_args: dict,
        nodes_selector: dict,
        cache: dict,
    ):
        super().__init__(
            regressor=regressor,
            reg_args=reg_args,
            cache=cache,
        )
        self.nodes_selector = nodes_selector
        self._cache = cache

    def align(self, u, v, g_u, g_v):
        nodes_selector = self.nodes_selector['cls'](
            **self.nodes_selector['args']
        )
        ref_nodes, scores = nodes_selector.select(
            u=u, v=v, g_u=g_u, g_v=g_v, scores_dict=self._cache
        )
        if not ref_nodes:
            return None, [], {}
        u_train = u.to_numpy(ref_nodes)
        v_train = v.to_numpy(ref_nodes)
        self.regressor.fit(v_train, u_train)
        return self.predict(v), ref_nodes, scores


class TemporalCentralityMeasureAligner(BaseAligner, AbstractDynAligner):

    def __init__(
        self,
        regressor: Type[OrthogonalProcrustesRegressor],
        reg_args: dict,
        percent: float,
        cache: dict,
        centrality_measure_name: str,
        verbose: bool,
    ):
        super().__init__(regressor=regressor, reg_args=reg_args, cache=cache)
        self._percent = percent
        self._verbose = verbose
        self._cache = cache

    def align(self, u, v, g_u, g_v):
        ref_nodes, scores = self.get_ref_nodes(g_u, g_v)

        u_train = u.to_numpy(ref_nodes)
        v_train = v.to_numpy(ref_nodes)

        self.regressor.fit(v_train, u_train)

        return self.predict(v), ref_nodes, scores

    def get_activity(self, g_u, nodes) -> Dict[int, float]:
        pass  # Not needed when cache is used

    def get_scores(
        self,
        a_u: Dict[int, float],
        a_v: Dict[int, float],
    ) -> Dict[int, float]:
        return {
            node: abs(a_u[node] - a_v[node])
            for node in a_u.keys()
        }

    def get_ref_nodes(self, g_u, g_v):
        common_nodes = [int(it) for it in get_common_nodes(g_u, g_v)]
        a_u, a_v = self._cache

        scores = self.get_scores(a_u=a_u, a_v=a_v)

        num_common_nodes = len(common_nodes)
        num_ref_nodes = int(self._percent * num_common_nodes)

        reference_nodes = sorted(scores.keys(), key=scores.get)
        reference_nodes = reference_nodes[:int(num_ref_nodes)]
        reference_nodes = [str(node) for node in reference_nodes]

        return reference_nodes, scores


def apply_aligner(
    aligner,
    cache_path, dataset_key,
    embeddings, embedding_key,
    graphs,
):
    first_embedding = embeddings[0]
    if isinstance(first_embedding, tuple):
        first_embedding = first_embedding[0]

    aligned_embeddings = [first_embedding]
    calculation_times = []
    ref_nodes = []
    scores = []

    for sid, (graph, embedding) in enumerate(zip(graphs[1:], embeddings[1:])):
        if isinstance(embedding, tuple):
            embedding = embedding[0]

        cache = read_cache(
            cache_path=cache_path,
            aligner_cfg=aligner,
            dataset_key=dataset_key,
            snapshot_idx=sid,
            embedding_key=embedding_key,
        )

        am = aligner['cls'](**aligner['cfg'], cache=cache)

        ae = None
        rfn = {}
        scr = {}
        calculation_time = np.nan

        unchanged, _ = get_unchanged_changed_nodes(
            src_graph=graphs[0],
            changed_graph=graph
        )
        if unchanged or aligner['name'] != 'ProcrustesUnchanged':
            aligner_st_time = timer()

            u_emb = embeddings[0]

            if isinstance(u_emb, tuple):
                u_emb = u_emb[0]

            ae, rfn, scr = am.align(
                u=u_emb,
                v=embedding,
                g_u=graphs[0],
                g_v=graph,
            )

            aligner_en_time = timer()
            calculation_time = aligner_en_time - aligner_st_time

        aligned_embeddings.append(ae)
        calculation_times.append(calculation_time)
        ref_nodes.append(rfn)
        scores.append(scr)

    return aligned_embeddings, calculation_times, ref_nodes, scores


def read_cache(
    cache_path: str,
    aligner_cfg: dict,
    dataset_key: str,
    snapshot_idx: int,
    embedding_key: str,
) -> Optional[dict]:
    with open(cache_path, 'rb') as fin:
        cache = pickle.load(fin)

    aligner_group = aligner_cfg['group']

    if 'Temporal' in aligner_group:
        centrality_measure_name = aligner_cfg['cfg']['centrality_measure_name']
        return cache[centrality_measure_name][dataset_key][snapshot_idx]
    elif aligner_group == 'EdgeJaccardPercent':
        selector_name = aligner_cfg['cfg']['nodes_selector']['cls'].__name__
        return cache[selector_name][dataset_key][snapshot_idx]
    elif aligner_group == 'EmbeddingNeighborJaccardPercent':
        selector_name = aligner_cfg['cfg']['nodes_selector']['cls'].__name__
        neighbors_percent = aligner_cfg['cfg']['nodes_selector']['args'][
            'neighbors_percent'
        ]
        return cache[selector_name][dataset_key][snapshot_idx][embedding_key][
            neighbors_percent
        ]
    else:
        return None


def align_all(all_graphs, all_embeddings, aligners, cache_path):
    results = {}

    for key, snapshots in all_graphs.items():
        graphs = snapshots['graphs']
        embeddings = all_embeddings[key]

        res = []

        for emb_run_id in tqdm(
            iterable=range(len(embeddings)),
            desc='Aligment - embedding retrain',
            leave=False,
        ):
            for aligner in aligners:
                ae, ct, rfn, sc = apply_aligner(
                    aligner=aligner,
                    cache_path=cache_path,
                    dataset_key=key,
                    embeddings=embeddings[f'embedding_{emb_run_id}'],
                    embedding_key=f'embedding_{emb_run_id}',
                    graphs=graphs,
                )

                res.append({
                    'run': emb_run_id,
                    'aligner': aligner['name'],
                    'aligned_embedding': ae,
                    'group': aligner['group'],
                    'reference_nodes': rfn,
                    'calculation_time': ct,
                    'scores': sc,
                })

        results[key] = res

    return results


def get_unchanged_changed_nodes(src_graph, changed_graph):
    unchanged = []
    changed = []
    common_nodes = [
        int(it) for it in get_common_nodes(src_graph, changed_graph)
    ]
    for node in common_nodes:
        src_edges = set(
            src_graph.edges([node])
        )
        changed_edges = set(
            changed_graph.edges([node])
        )
        if src_edges == changed_edges:
            unchanged.append(node)
        else:
            changed.append(node)

    return set(unchanged), set(changed)


def get_common_nodes(g_u, g_v):
    nodes = set(g_u.nodes()).intersection(g_v.nodes())
    nodes = sorted(nodes)
    nodes = list(map(str, nodes))
    return nodes