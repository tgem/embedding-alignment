from typing import Union, List

import numpy as np
import scipy
from tqdm.auto import tqdm

from dynalign.embeddings import KeyedModel


def ped_score(
    embedding: KeyedModel, aligned_embedding: KeyedModel,
    batch_size: int = 8, verbose: bool = False,
) -> float:
    nodes = embedding.nodes
    node2idx = dict(zip(nodes, range(len(nodes))))

    embedding = embedding.to_numpy(nodes)
    aligned_embedding = aligned_embedding.to_numpy(nodes)

    ped = 0

    # Obtain batched indexes
    idxs = [node2idx[n] for n in nodes]
    idxs = [idxs[i:i + batch_size] for i in range(0, len(idxs), batch_size)]

    for u in tqdm(idxs, desc='PED batches', leave=False, disable=not verbose):
        f_dist = scipy.spatial.distance.cdist(
            embedding[u],
            embedding,
            metric='euclidean',
        )

        f_star_dist = scipy.spatial.distance.cdist(
            aligned_embedding[u],
            aligned_embedding,
            metric='euclidean',
        )
        ped += np.sum(np.abs(f_dist - f_star_dist))

    ped /= (len(nodes) * (len(nodes) - 1))
    return ped


def ssd_score(
    embedding: KeyedModel, aligned_embedding: KeyedModel,
    reference_nodes: Union[List[str], List[int]], batch_size: int = 8,
    verbose: bool = False,
) -> float:
    nodes = embedding.nodes
    node2idx = dict(zip(nodes, range(len(nodes))))

    embedding = embedding.to_numpy(nodes)
    aligned_embedding = aligned_embedding.to_numpy(nodes)

    # Reference nodes ratios
    ssd_ref = 0

    rn_idxs = [node2idx[rn] for rn in reference_nodes]
    for u_r in tqdm(rn_idxs, desc='SSD batches rn', leave=False,
                    disable=not verbose):
        f_star_dist = scipy.spatial.distance.cdist(
            [aligned_embedding[u_r]],
            aligned_embedding[rn_idxs],
            metric='euclidean'
        )
        f_dist = scipy.spatial.distance.cdist(
            [embedding[u_r]],
            embedding[rn_idxs],
            metric='euclidean'
        )

        ratios = f_star_dist / f_dist

        ratios[np.isinf(ratios)] = 0.
        ratios[np.isnan(ratios)] = 0.

        ssd_ref += np.sum(ratios)

    ssd_ref /= (len(reference_nodes) * (len(reference_nodes) - 1))

    # Other nodes
    ssd_other = 0

    idxs = [node2idx[n] for n in nodes if n not in set(reference_nodes)]
    if not idxs:
        return np.nan

    batch_idxs = [idxs[i:i + batch_size] for i in
                  range(0, len(idxs), batch_size)]

    for u in tqdm(batch_idxs, desc='SSD batches other', leave=False,
                  disable=not verbose):
        f_dist = scipy.spatial.distance.cdist(
            embedding[u],
            embedding[idxs],
            metric='euclidean',
        )

        f_star_dist = scipy.spatial.distance.cdist(
            aligned_embedding[u],
            aligned_embedding[idxs],
            metric='euclidean',
        )

        ratios = f_star_dist / f_dist
        ratios[np.isinf(ratios)] = 0.
        ratios[np.isnan(ratios)] = 0.

        ssd_other += np.sum(ratios)

    ssd_other /= (len(idxs) * (len(idxs) - 1))

    ssd = np.abs(ssd_ref - ssd_other)
    return ssd


def rnd_score(
    reference_embedding: KeyedModel, aligned_embedding: KeyedModel,
    reference_nodes: List[str], verbose: bool = False,
) -> float:
    r_node2idx = dict(zip(
        reference_embedding.nodes,
        range(len(reference_embedding.nodes))
    ))
    a_node2idx = dict(zip(
        aligned_embedding.nodes,
        range(len(aligned_embedding.nodes))
    ))

    reference_embedding = reference_embedding.to_numpy(
        reference_embedding.nodes
    )
    aligned_embedding = aligned_embedding.to_numpy(aligned_embedding.nodes)

    # Reference nodes ratios
    rnd = 0

    for u_r in tqdm(reference_nodes, desc='RND batches', leave=False,
                    disable=not verbose):
        rnd += np.sqrt(
            np.sum(
                np.power(
                    (
                        reference_embedding[r_node2idx[u_r]] -
                        aligned_embedding[a_node2idx[u_r]]
                    ), 2
                ),
                axis=-1
            )
        )

    rnd /= len(reference_nodes)

    return rnd