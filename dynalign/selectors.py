import math
from abc import ABC, abstractmethod
from typing import Optional

import torch

from dynalign.aligners import get_common_nodes


def get_percent_of_nodes_from_scores_dict(scores_dict, percent):
    if percent < 0 or percent > 1:
        raise ValueError("Wrong percent value")
    n = math.ceil(len(scores_dict) * percent)
    reference_nodes = sorted(scores_dict.keys(), key=scores_dict.get)
    reference_nodes = reference_nodes[:int(n)]
    reference_nodes = [str(node) for node in reference_nodes]
    return reference_nodes


def get_node_mapping(nodes, common_nodes):
    mapping = {}
    for node in common_nodes:
        mapping[node] = nodes.index(node)

    return mapping


class AbstractNodesSelector(ABC):
    SELECTION_METHODS = {
        'percent': get_percent_of_nodes_from_scores_dict,
    }

    def __init__(
        self,
        selection_method: Optional[str] = None,
        selection_method_args: Optional[dict] = None,
    ):
        self.selection_method = None
        self.selection_method_args = None

        if isinstance(selection_method, str):
            if selection_method in self.SELECTION_METHODS.keys():
                self.selection_method = self.SELECTION_METHODS[
                    selection_method
                ]
            else:
                raise ValueError(
                    f'Selection method {selection_method} not implemented. '
                    f'Try {self.SELECTION_METHODS}'
                )
            self.selection_method_args = selection_method_args

    @abstractmethod
    def get_scores(self, u, v, g_u, g_v):
        pass

    def select(self, u, v, g_u, g_v, scores_dict=None):
        if not self.selection_method:
            raise ValueError(
                'Node selection function undefined. '
                'Pass selection method name.'
            )
        if not scores_dict:
            scores_dict = self.get_scores(u=u, v=v, g_u=g_u, g_v=g_v)
        ref_nodes = self.selection_method(
            scores_dict=scores_dict,
            **self.selection_method_args
        )
        return ref_nodes, scores_dict


class EdgeJaccardNodesSelector(AbstractNodesSelector):
    def __init__(
        self,
        selection_method: Optional[str] = None,
        selection_method_args: Optional[dict] = None
    ):
        super(EdgeJaccardNodesSelector, self).__init__(
            selection_method, selection_method_args
        )

    def get_scores(self, u, v, g_u, g_v):
        common_nodes = get_common_nodes(g_u, g_v)

        scores_dict = {}
        for node in common_nodes:
            node_gu_neighbors = set(g_u.neighbors(int(node)))
            node_gv_neighbots = set(g_v.neighbors(int(node)))
            common_neighbors = len(
                node_gu_neighbors.intersection(node_gv_neighbots)
            )
            denominator_neighbors = len(
                node_gu_neighbors.union(node_gv_neighbots)
            )
            if denominator_neighbors != 0:
                scores_dict[node] = 1 - (
                    common_neighbors / denominator_neighbors
                )
            else:
                scores_dict[node] = 1

        return scores_dict


class EmbeddingNeighborJaccard(AbstractNodesSelector):
    def __init__(
        self,
        neighbors_percent: float,
        selection_method: Optional[str] = None,
        selection_method_args: Optional[dict] = None
    ):
        super().__init__(selection_method, selection_method_args)
        self.neighbors_percent = neighbors_percent

    def get_scores(self, u, v, g_u, g_v):
        u_nodes = set(u.nodes)
        v_nodes = set(v.nodes)
        common_nodes = u_nodes.intersection(v_nodes)

        u_nodes = sorted(u_nodes, key=lambda x: int(x))
        v_nodes = sorted(v_nodes, key=lambda x: int(x))
        common_nodes = sorted(common_nodes, key=lambda x: int(x))

        u_mapping = get_node_mapping(u_nodes, common_nodes)
        v_mapping = get_node_mapping(v_nodes, common_nodes)

        assert common_nodes == list(u_mapping.keys())
        assert list(u_mapping.keys()) == list(v_mapping.keys())

        u_emb = torch.tensor(u.to_numpy(u_nodes))
        v_emb = torch.tensor(v.to_numpy(v_nodes))

        u_dists = torch.cdist(u_emb[list(u_mapping.values())], u_emb)
        v_dists = torch.cdist(v_emb[list(v_mapping.values())], v_emb)

        nodes_count = math.ceil(len(common_nodes) * self.neighbors_percent)
        if nodes_count != len(v_nodes) and nodes_count != len(u_nodes):
            nodes_count += 1

        v_neighbors = torch.topk(v_dists, k=nodes_count, largest=False).indices
        u_neighbors = torch.topk(u_dists, k=nodes_count, largest=False).indices

        scores_dict = {}

        for node_id in range(len(common_nodes)):
            n_u_neighbors = set(
                [u_nodes[it] for it in u_neighbors[node_id].numpy()[1:]]
            )
            n_v_neighbors = set(
                [v_nodes[it] for it in v_neighbors[node_id].numpy()[1:]]
            )
            scores_dict[common_nodes[node_id]] = 1 - (
                    len(n_u_neighbors.intersection(n_v_neighbors)) /
                    len(n_u_neighbors.union(n_v_neighbors))
            )

        return scores_dict
