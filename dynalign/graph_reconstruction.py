from typing import Dict, Tuple

import networkx as nx
import numpy as np


def map_score_batch(
    graph: nx.Graph, emb_dists: np.ndarray, batch_metadata: Dict[str, int],
) -> Tuple[float, float]:
    """Calculates mean average precision on embeddings."""

    _map = 0.0
    _nodes_with_neighbors = 0

    batch_nb_nodes = batch_metadata['batch_nb_nodes']
    batch_offset = batch_metadata['batch_first_id']

    for i in range(batch_nb_nodes):
        graph_node_id = i + batch_offset
        neighbors = list(graph.neighbors(graph_node_id))

        if len(neighbors) > 0 and neighbors != [graph_node_id]:
            dists = emb_dists[i]
            dists[graph_node_id] = 1e6
            ranks = np.argsort(np.argsort(dists)) + 1

            neighbors_ranks = np.sort([
                ranks[nid]
                for nid in neighbors
                if nid != graph_node_id
            ])
            precision = [
                (j + 1) / rank
                for j, rank in enumerate(neighbors_ranks)
            ]

            _map += np.average(precision)
            _nodes_with_neighbors += 1

    return _map, _nodes_with_neighbors


def distortion_batch(
    graph_dists: np.ndarray, emb_dists: np.ndarray,
) -> Tuple:
    """Calculates distortion metric between embedding and original graph."""
    assert len(graph_dists) == len(emb_dists), "Distances matrices " \
                                               "have to be equal length"
    total_pairs = graph_dists.shape[0] * (graph_dists.shape[1] - 1)

    np.fill_diagonal(graph_dists, 1)
    np.fill_diagonal(emb_dists, 1)

    indices = np.where(graph_dists == 0)
    graph_dists[indices] = 1
    emb_dists[indices] = 1
    total_pairs -= len(indices[0])

    d_sum = np.sum((np.abs(emb_dists - graph_dists)) / graph_dists)
    return d_sum, total_pairs




